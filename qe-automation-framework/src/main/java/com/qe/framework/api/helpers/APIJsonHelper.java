package com.qe.framework.api.helpers;

import static io.restassured.RestAssured.given;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.qe.framework.api.json.JsonReaderCommon;
import com.qe.framework.common.FrameWorkHelper;
import com.qe.framework.common.PropertiesHelper;

import io.restassured.http.ContentType;
import io.restassured.http.Cookies;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class APIJsonHelper {
	private static final Logger logger = Logger.getLogger(APIJsonHelper.class);
	public static Response response;
	public static PropertiesHelper loadProps = PropertiesHelper.getInstance();
	private String errorTxt;
	public static String apiEndpointIP;
	public static String apiEndpointWebURL;
	public static io.restassured.http.Cookies httpCookies;
	public static Map<String, String> authcookies = new HashMap<String, String>();
	public static String accessToken;
	

	public APIJsonHelper(){
		loadProps = PropertiesHelper.getInstance();
		apiEndpointIP= System.getProperty("apiEndpointIP");
		if(apiEndpointIP ==null || apiEndpointIP.isEmpty()){
			apiEndpointIP = loadProps.getConfigPropProperty("api.baseURL");
		}
		apiEndpointWebURL = System.getProperty("apiEndpointWebURL");
		if(apiEndpointWebURL ==null || apiEndpointWebURL.isEmpty()){
			apiEndpointWebURL = loadProps.getConfigPropProperty("api.uat6int.baseURL");
		}

		logger.debug("API Endpoint IP Address:"+apiEndpointIP);
		logger.debug("API Endpoint WEB Address:"+apiEndpointWebURL);
	}

	public Response initiateRestAPICall(String url){
		//"http://35.224.100.42/api/category?categoryIds=15750"
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().when().get(url);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
public Response initiateRestAPICall(String url, String accessToken){
		
		try{
			response = given().relaxedHTTPSValidation().contentType("application/json").header("Authorization", accessToken).get(url);
			System.out.println("response" + response);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	
public Response initiateRestAPICallWithAcessGetCookie(String url){
		
		try{
			response = given().relaxedHTTPSValidation().contentType("application/json").cookies(authcookies).get(url);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public Response initiateRestAPICallWithAcessToken(String url){
		
		try{
			url = urlAddDebug(url);
			
			response = given().relaxedHTTPSValidation().header("Content-Type", "application/json").header("Authorization", accessToken).get(url);
			System.out.println("response" + response);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	

	public Response initiateRestAPICallWithCookie(String url){
		//"http://35.224.100.42/api/category?categoryIds=15750"
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().cookies(httpCookies).contentType("application/json").get(url);
			logger.debug("Response:::"+ response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestDeleteAPICall(String url){
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().when().cookies(httpCookies).contentType("application/json").delete(url);
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestAPICallWithCookies(String url,List<io.restassured.http.Cookie> restAssuredCookies){
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().contentType(ContentType.JSON).cookies(new Cookies(restAssuredCookies)).get(url);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICallWithCookies exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}


	
	public Response initiateRestPostAPICallWithCookies(String url,List<io.restassured.http.Cookie> restAssuredCookies,String jsonRequestFilePath){
		try{
			url = urlAddDebug(url);
			
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("POST Request JSON:"+postRequestStr);
			response = given().relaxedHTTPSValidation().contentType(ContentType.JSON).body(postRequestStr).cookies(new Cookies(restAssuredCookies)).post(url);
			//response = given().contentType(ContentType.JSON).body(postRequestStr).cookies(new Cookies(restAssuredCookies)).post(url);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICallWithCookies exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPutAPICallWithCookies(String url,List<io.restassured.http.Cookie> restAssuredCookies,String jsonRequestFilePath){
		try{
			url = urlAddDebug(url);
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("PUT Request JSON:"+postRequestStr);
			response = given().relaxedHTTPSValidation().contentType(ContentType.JSON).body(postRequestStr).cookies(new Cookies(restAssuredCookies)).put(url);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPutAPICallWithCookies exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestAPICallWithGuestUserCookies(String url){
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().cookies(httpCookies).contentType("application/json").when().get(url);
			logger.debug("JSON Response::"+response.asString());

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPostAPICallWithoutSignIn(String url,List<io.restassured.http.Cookie> restAssuredCookies,String jsonRequestFilePath){
		System.setProperty("OrderId", "");
		System.setProperty("ItemId",  "");
		try{
			url = urlAddDebug(url);
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("POST Request JSON:"+postRequestStr);
			response = given().relaxedHTTPSValidation().contentType("application/json").body(postRequestStr).cookies(new Cookies(restAssuredCookies)).post(url);
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies(); //given().contentType("application/json").when().body(postRequestStr).post(url).then().statusCode(200).extract().response().getDetailedCookies();

			JsonPath jsonPathEvaluator = response.jsonPath();
			String  orderID = jsonPathEvaluator.get("addToCart.orderId[0]");
			String itemID = jsonPathEvaluator.get("addToCart.items[0].itemId");
			logger.debug("Order ID::"+ orderID);
			logger.debug("Item ID::"+ itemID);
			System.setProperty("OrderId", orderID);
			System.setProperty("ItemId",  itemID);

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICallWithoutSignIn exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response changePasswordLogInAPICall(String url,String updateRequestStr){
		try{
			url = urlAddDebug(url);
			logger.debug("Request JSON:"+updateRequestStr);
			response = given().relaxedHTTPSValidation().contentType("application/json").body(updateRequestStr).post(url);
			httpCookies =  response.getDetailedCookies();
			logger.debug("JSON Response::"+response.asString());

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPutAPICallWithCookies exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestAPIPostCallForGuestAuthen(String url){
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().contentType("application/json").post(url);
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies();//given().contentType("application/json").when().post(url).then().statusCode(200).extract().response().getDetailedCookies();
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICallWithCookies exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;		
	}
	

	public Response initiateRestPostAPICallWithoutCookiesAndReqStr(String url,String regRequestStr){
		try{
			url = urlAddDebug(url);
			logger.debug("POST Request JSON:"+regRequestStr);
			response = given().relaxedHTTPSValidation().contentType("application/json").body(regRequestStr).post(url);
			logger.debug("JSON Response::"+response.asString());
			//httpCookies =  given().contentType("application/json").when().body(postRequestStr).post(url).then().statusCode(200).extract().response().getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPostAPICallWithoutCookies(String url,String jsonRequestFilePath){
		try{
			url = urlAddDebug(url);
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("POST Request JSON:"+postRequestStr);
			response = given().relaxedHTTPSValidation().contentType("application/json").body(postRequestStr).post(url);
			logger.debug("JSON Response::"+response.asString());
			//httpCookies =  given().contentType("application/json").when().body(postRequestStr).post(url).then().statusCode(200).extract().response().getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	

	public Response initiateRestPostAPICallWithAcessCookie(String url,String jsonRequestFilePath){
		try{
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("POST Request JSON:"+postRequestStr);
			response = given().relaxedHTTPSValidation().header("Content-Type", "application/json").cookies(authcookies).body(postRequestStr).post(url);
		
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public Response initiateRestPostAPICallWithAcessToken(String url,String jsonRequestFilePath){
		try{
			url = urlAddDebug(url);
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("POST Request JSON:"+postRequestStr);
						
			response = given().relaxedHTTPSValidation().header("Content-Type", "application/json").header("Authorization", accessToken).body(postRequestStr).post(url);
			
			System.out.println( "response.getStatusCode()======>" + response.getStatusCode());
			
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies();//given().relaxedHTTPSValidation().contentType("application/json").when().body(postRequestStr).post(url).then().statusCode(200).extract().response().getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public Response initiateRestPostAPICall(String url,String jsonRequestFilePath){
        try{
               url = urlAddDebug(url);
               String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
               logger.debug("POST Request JSON:"+postRequestStr);
               response = given().relaxedHTTPSValidation().contentType("application/json").body(postRequestStr).post(url);
               logger.debug("JSON Response::"+response.asString());
               httpCookies =  response.getDetailedCookies();//given().relaxedHTTPSValidation().contentType("application/json").when().body(postRequestStr).post(url).then().statusCode(200).extract().response().getDetailedCookies();              
        }catch (Exception e) {
               response = null;
               logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
               e.printStackTrace();
        }
        return response;
 }

	
	public Response initiateRestPutAPICallWithAcessToken(String url,String jsonRequestFilePath){
		try{
			url = urlAddDebug(url);
			String putRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("PUT Request JSON:"+putRequestStr);
			response = given().relaxedHTTPSValidation().header("Content-Type", "application/json").header("Authorization", accessToken).body(putRequestStr).put(url);
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public Response initiateRestPutAPICallWithAcessCookie(String url,String jsonRequestFilePath){
		try{
			String putRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("PUT Request JSON:"+putRequestStr);
			
			response = given().relaxedHTTPSValidation().header("Content-Type", "application/json").cookies(authcookies).body(putRequestStr).put(url);
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPutAPICall(String url,String jsonRequestFilePath){
		try{
			String putRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("PUT Request JSON:"+putRequestStr);
			response = given().relaxedHTTPSValidation().header("Content-Type", "application/json").header("Authorization", accessToken).body(putRequestStr).put(url);
			logger.debug("JSON Response::"+response.asString());
			httpCookies =  response.getDetailedCookies();		
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	public Response initiateRestPostAPICallWithCookies(String url,String jsonRequestFilePath){
		try{
			url = urlAddDebug(url);
			String postRequestStr = JSONValidationUtils.convertJsonFileToString(JsonReaderCommon.jsonRequestFolderPath+ jsonRequestFilePath+".json");
			logger.debug("POST Request JSON:"+postRequestStr);
			response = given().relaxedHTTPSValidation().cookies(httpCookies).contentType("application/json").body(postRequestStr).post(url);
			logger.debug("JSON Response::"+response.asString());

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPostAPICallWithCookiesAndRequestJsonStr(String url,String jsonRequestStr){
		try{
			url = urlAddDebug(url);
			logger.debug("POST Request JSON:"+jsonRequestStr);
			response = given().relaxedHTTPSValidation().cookies(httpCookies).contentType("application/json").body(jsonRequestStr).post(url);
			logger.debug("POST Response JSON::"+response.asString());

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPostAPICallWithCookiesAndWithOutBody(String url){
		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().cookies(httpCookies).contentType("application/json").post(url);
			logger.debug("POST Response JSON::"+response.asString());

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	public Response initiateUpdateRestPostAPICallWithCookies(String url,String jsonRequestStr){
		try{
			url = urlAddDebug(url);
			logger.debug("POST Request JSON:"+jsonRequestStr);
			response = given().relaxedHTTPSValidation().cookies(httpCookies).contentType("application/json").body(jsonRequestStr).post(url);
			logger.debug("JSON Response::"+response.asString());

		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Response initiateRestPostAPICallWithoutBody(String url){

		try{
			url = urlAddDebug(url);
			response = given().relaxedHTTPSValidation().contentType("application/json").post(url);
			logger.debug("JSON Response::"+response.asString());
		}catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICallWithCookies exception msg::"+e.getMessage());
			e.printStackTrace();
		}
		return response;
	}


	public String initiateErrorRestAPICall(String strURL){

		try{
			URL url = new URL(strURL);//"http://35.202.244.154/api/categorycategoryIds={15613,15157,15645}");//"http://35.202.244.154/15613,15157,15645");

			URLConnection urlc = url.openConnection();
			urlc.setDoOutput(true);
			urlc.setAllowUserInteraction(false);

			PrintStream ps = new PrintStream(urlc.getOutputStream());
			ps.close();

			BufferedReader br = new BufferedReader(new InputStreamReader(urlc
					.getInputStream()));
			String l = null;
			while ((l=br.readLine())!=null) {
				logger.debug(l);
			}
			br.close();
		}catch (Exception e) {
			errorTxt = e.getMessage();
			logger.error("##### initiateErrorRestAPICall() inside Exception msg :: "+e.getMessage());
		}
		return errorTxt;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		APIJsonHelper.response = response;
	}

	public String toString()
	{
		return APIJsonHelper.response.asString() ;
	}

	public int getStatusCode()
	{
		return APIJsonHelper.response.statusCode() ;
	}

	public String getStausLine()
	{
		return APIJsonHelper.response.statusLine();
	}

	public String getContentType()
	{
		return APIJsonHelper.response.contentType();
	}

	public String getHeader(String expectedHeader)
	{
		return APIJsonHelper.response.header(expectedHeader);
	}

	public Headers getHeaders()
	{
		return APIJsonHelper.response.headers();
	}

	public ResponseBody<?> getResponseBody()
	{
		return APIJsonHelper.response.getBody();
	}

	public String getResponseBodyToString()
	{
		return APIJsonHelper.response.getBody().asString();
	}

	public String getErrorTxt() {
		return errorTxt;
	}

	public void setErrorTxt(String errorTxt) {
		this.errorTxt = errorTxt;
	}


	public String getValueFromResponse(String jsonPath){
		String value ="";
		try{
			if(response != null){
				JsonPath jsonPathEvaluator = response.jsonPath();
				value = jsonPathEvaluator.get(jsonPath);
			}

		}catch (Exception e) {
			logger.error("Exception Msg::"+e.getMessage());
		}
		logger.debug("Value From Json Response::"+ value);
		return value;
	}

	public JSONObject getFirstAddressDetails() {

		JSONParser parser = new JSONParser();
		JSONObject jsonObject =null;
		try {
			if(response != null){
				Object rootObj = parser.parse(getResponseBodyToString());

				JSONArray jsonArray = (JSONArray) rootObj;
				for(Object obj:jsonArray){
					jsonObject =  (JSONObject) obj;
					break;
				}
			}
		}catch (Exception e) {
			logger.error("Exception Msg::"+e.getMessage());
		}

		return jsonObject ;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getShippingAddressDetails() {

		JSONParser parser = new JSONParser();
		JSONObject jsonObject =null;
		try {
			if(response != null){
				Object rootObj = parser.parse(getResponseBodyToString());

				JSONObject rootjsonObj =  (JSONObject) rootObj;
				JSONObject profile=(JSONObject) rootjsonObj.get("profile");

				JSONArray sortByInfo = (JSONArray) profile.get("address");
				for (Object sortByInfoObj : sortByInfo) {
					jsonObject = (JSONObject) sortByInfoObj;
					String addressType = (String) jsonObject.get("addressType");
					if(addressType != null && "shipping".equalsIgnoreCase(addressType)){
						jsonObject.put("lastName", FrameWorkHelper.getRandomAlphabetic(8));
						jsonObject.put("firstName", FrameWorkHelper.getRandomAlphabetic(8));
						jsonObject.put("address", FrameWorkHelper.getRandomAlphabetic(14));
						break;
					}
				}
			}
		}catch (Exception e) {
			logger.error("Exception Msg::"+e.getMessage());
		}

		return jsonObject ;
	}
	private String urlAddDebug(String url){

		try{
			if(url!= null){
				if(url.contains("?")) {
					url = url + "&debug=aso";
				} else {
					url = url + "?debug=aso";
				}
			}
		}catch (Exception e) {
			logger.error("Exception Msg::"+e.getMessage());
		}
		logger.debug("EndPoint URL::"+url);
		return url;
	}
}
