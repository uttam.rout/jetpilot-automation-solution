package com.qe.framework.common;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

//import range.service.integration.test.GetAccessTokenForBDD;

public class JsonValidation {
	private static Logger logger = Logger.getLogger(JsonValidation.class);

	private static String returnValueFromJSonaArray(final JSONArray currentJson, final String inputKey)
			throws JSONException {
		String valueOfInputKey = null;
		for (int i = 0; i < currentJson.length(); i++) {
			JSONObject explrObject = currentJson.getJSONObject(i);
			try {
				valueOfInputKey = (String) explrObject.get(inputKey);
			} catch (ClassCastException e) {
				int valOfKey = explrObject.getInt(inputKey);
				valueOfInputKey = Integer.toString(valOfKey);
			}
		}
		return valueOfInputKey;
	}

	public static int getCountOfElementsOfNestedJsonArray(final String jsonString, final String parentKey) {

		int countOfElements = 0;
		try {
			JSONObject responseJson = new JSONObject(jsonString);
			JSONArray currentJsonArray = responseJson.getJSONArray(parentKey);
			countOfElements = currentJsonArray.length();
		} catch (JSONException e) {
		}
		return countOfElements;

	}

	public static String getValueFromJSonArray(final String outputJSon, final String inputKey) {
		String valueOfInputKey = null;
		try {
			JSONArray responseJson = new JSONArray(outputJSon);
			System.out.println("Responsejoson ==>Inside " + responseJson);
			valueOfInputKey = returnValueFromJSonaArray(responseJson, inputKey);
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;

	}

	public static String getValueFromJsonNestedArray(final String outputJSon, final String inputKey) {
		String valueOfInputKey = null;
		try {

			JSONObject responseJson = new JSONObject(outputJSon);
			JSONArray currentJson = responseJson.getJSONArray("data");
			valueOfInputKey = returnValueFromJSonaArray(currentJson, inputKey);
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;

	}

	public static String getValueFromJsonNestedArray(final String outputJSon, final String parentKey,
			final String inputKey) {
		String valueOfInputKey = null;
		try {

			JSONObject responseJson = new JSONObject(outputJSon);
			JSONArray currentJson = responseJson.getJSONArray(parentKey);
			valueOfInputKey = returnValueFromJSonaArray(currentJson, inputKey);
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;

	}

	public static String getValueFromJsonNestedArray(final String outputJSon, final String parentKey,
			final String inputKey, final int index) {
		String valueOfInputKey = null;
		try {

			JSONObject responseJson = new JSONObject(outputJSon);
			JSONArray currentJsonArray = responseJson.getJSONArray(parentKey);
			JSONObject currentJson = (JSONObject) currentJsonArray.get(index);
			valueOfInputKey = (String) currentJson.get(inputKey);
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;
	}

	public static String getValueFromJSonArray(final String outputJSon, final String inputKey, final int index) {
		String valueOfInputKey = null;
		try {
			JSONArray responseJson = new JSONArray(outputJSon);
			JSONObject currentJson = (JSONObject) responseJson.get(index);
			valueOfInputKey = (String) currentJson.get(inputKey);
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;

	}

	public static String getValueFromJSon(final String outputJSon, final String inputKey) {
		String valueOfInputKey = null;
		try {
			JSONObject currentJson = new JSONObject(outputJSon);
			valueOfInputKey = (String) currentJson.get(inputKey);
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;

	}

	public static String getValueFromNestedJSon(final String outputJSon, final String parentKey,
			final String inputKey) {
		String valueOfInputKey = null;
		try {
			JSONObject currentJson = new JSONObject(outputJSon);
			currentJson = currentJson.getJSONObject(parentKey);
			valueOfInputKey = String.valueOf(currentJson.get(inputKey));
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return valueOfInputKey;

	}

	public static boolean validateOutputJSon(final String outputJSon) {
		try {
			new JSONObject(outputJSon);
		} catch (JSONException e) {
			return false;
		}
		return true;
	}

	public static boolean validateOutputJSonArray(final String outputJSon) {
		try {

			new JSONArray(outputJSon);
			return true;
		} catch (JSONException e) {
			return false;
		}
	}

	public static int getCountOfElementsInJSonArray(final String outputJSon) {
		int countOfElements = 0;
		try {
			JSONArray responseJson = new JSONArray(outputJSon);
			countOfElements = responseJson.length();
		} catch (JSONException e) {
			logger.debug("not a valid json array", e);
		}
		return countOfElements;

	}

	public static <T> String marshalResponse(final String output, Class<T> classType) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		List<T> storeOverlays = objectMapper.readValue(output, List.class);
		return objectMapper.writeValueAsString(storeOverlays);
	}

	public boolean validateKeyInJSonArray(final String outputJSon, final String inputKey) {
		boolean keyExist = false;
		try {
			JSONArray responseJson = new JSONArray(outputJSon);
			JSONObject currentJson = (JSONObject) responseJson.get(0);
			currentJson.get(inputKey);
			keyExist = true;
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}

		return keyExist;
	}

	public static boolean validateKeyInJSon(final String outputJSon, final String inputKey) {
		boolean keyExist = false;
		try {
			JSONObject currentJson = new JSONObject(outputJSon);
			currentJson.get(inputKey);
			keyExist = true;
		} catch (JSONException e) {
			logger.debug(e.getMessage());
		}
		return keyExist;

	}

}
