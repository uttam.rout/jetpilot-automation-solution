package com.qe.framework.common;

public class Constants {
	
	public static final String PASS = "PASS";
			 
    public static final String FAIL = "FAIL";
    
    public static final String PARTIALLYPASS = "Partially PASS";
    
    public static String API_ERROR_DESCRIPTION = "";
    public static String API_ERROR_TEXT = " Property Key Not found or value is null due to fail.";
    public static final String DRIVERSROOTFOLDERPATH =  System.getProperty("user.dir")+"/src/test/resources/drivers/";
    public static String DRIVERSFOLDERPATH ;
    public static String API_AUTOSUGGESTION_ERROR_TEXT = " auto suggestion response text not containg with ";
    public static String screenShortTagNames;
    public static boolean isBrowserProxyEnabled = false;
    public static String enableBrowserProxy;
    
    public static int thread_highest;
	public static int thread_high;
	public static int thread_medium;
	public static int thread_low;
}
