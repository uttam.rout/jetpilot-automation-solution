/**
 * 
 */
package com.qe.framework.common;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;


/**
 * @author ravthota
 *
 */
public class FrameWorkHelper {
	private static final Logger logger = Logger.getLogger(FrameWorkHelper.class);
	
	public static String getRandomAlphabetic(int randomStrLength){
		logger.debug("");
		return RandomStringUtils.randomAlphabetic(randomStrLength);
	}

	public static String getRandomAlphanumeric(int randomStrLength){
		return RandomStringUtils.randomAlphanumeric(randomStrLength);
	}
	
	
	public static String getRandomNumber(int randomIntLength){
		return RandomStringUtils.randomNumeric(randomIntLength);
	}
	
}
