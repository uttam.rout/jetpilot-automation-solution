package com.qe.framework.db.helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.qe.framework.common.PropertiesHelper;

public class SqlDatabaseManager 
{
	public static Connection  connection; 
	public static Statement statement; 
	private static final Logger logger = Logger.getLogger(SqlDatabaseManager.class);
	
	public SqlDatabaseManager(String DBurl) throws ClassNotFoundException 
		{
			String url = DBurl;
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		try 
		{ 
			connection = DriverManager.getConnection(url); // username and password are passed into
			statement = connection.createStatement();
			System.out.println("Test is passed successfully");
			logger.info("Connection is test successfully");
			
		} 
		catch (SQLException exception) 
		{
			System.out.println("\n*** SQLException caught ***\n");
			while (exception != null) { // tell us the problem
			System.out.println("SQLState:    " + exception.getSQLState());
			System.out.println("Message:     " + exception.getMessage());
			System.out.println("Error code:  " + exception.getErrorCode());
			exception = exception.getNextException();
			System.out.println("");
			logger.info("Connection is Failed");
			}
		} 
		catch (java.lang.Exception exception) 
		{
			exception.printStackTrace();
		}
		
		}

	public static Statement getStatement() {
		return statement;
	}

	public static void setStatement(Statement statement) {
		SqlDatabaseManager.statement = statement;
	}

	public static Connection getConnection() {
		return connection;
	}

	public static void setConnection(Connection connection) {
		SqlDatabaseManager.connection = connection;
	}
	
	

}
