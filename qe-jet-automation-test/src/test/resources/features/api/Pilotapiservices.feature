Feature: API Test execution for JetPilot

  @ApiWebservices
  Scenario: JetPilot Sample Test API services
    Given load the url "http://product.apps.jetsdev.psteklabs.com/product/name/jam"
    When hit url
    Then Verify the values "ID12" and "Canned Food" and "Preservatives" in json response
