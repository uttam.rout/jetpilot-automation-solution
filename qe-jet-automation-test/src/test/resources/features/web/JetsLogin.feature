
@Jets_Login
Feature: As an Existing Customer, I should be able to login to schedule service request.

  Background: 
    Given customer is on Jets.com
    And customer wants to signIn

  Scenario: SignIn form have the required fields
    Then the SignIn page contains all required fields

Scenario: SignIn form username support alphanumeric and special characters
    Then the username textbox support alphanumeric characters
    And the username textbox support special characters

Scenario: SignIn form password support alphanumeric and special characters
    Then the password textbox support alphanumeric characters
    And the password textbox support special characters
    And the password textbox support masked characters

  Scenario: Check SignIn form landing on valid inputs
    When customer login with valid inputs
    Then customer is directed to 'Schedule an Appointment' screen

  Scenario: Check SignIn form landing on invalid username
    When customer login with invalid username
    Then customer is given error as "No account with that username."

Scenario: Check SignIn form landing on empty username
    When customer login with empty username
    Then customer is given error

  Scenario: Check SignIn form landing on invalid password
    When customer login with invalid password
    Then customer is given error as "Incorrect password entered."
    
    Scenario: Check SignIn form landing on empty password
    When customer login with empty password
    Then customer is given error
