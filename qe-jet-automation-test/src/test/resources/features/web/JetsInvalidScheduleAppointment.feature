@Jets_Schedule_Appointment
Feature: Check Jets schedule an appointment

  #As the Customer, I should not be able to schedule an appointment for a given date and time, if that time slot is already taken
  Background: 
    Given customer is on Jets.com
    And customer login with valid inputs
    And customer wants to schedule appointment for the first product in list

  Scenario: Check appointment page elements
    Then appointment page contain all required fields with default values

  Scenario: Check user is not able to take already taken appointment
    When customer tried for an occupied appointment
    Then booking of that appointment should be disabled
