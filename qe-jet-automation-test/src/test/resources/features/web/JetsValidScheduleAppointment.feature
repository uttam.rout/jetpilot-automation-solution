@Jets_Schedule_Appointment
Feature: Check Jets schedule an appointment

  #As the Customer, I should not be able to schedule an appointment for a given date and time, if that time slot is already taken
  Background: 
    Given customer is on Jets.com
    And customer login with valid inputs
    And customer wants to schedule appointment for the first product in list

  Scenario: Check appointment page elements
    Then appointment page contain all required fields with default values

  Scenario: Check options in time field for scheduling appointment
    Then time has options as "Morning", "Mid-day" and "Afternoon"

  Scenario: Check options in location field for scheduling appointment
    Then location has options as "Mid-Town, NYC", "Downtown, NYC", "Long Island, NYC" and "Downtown, Chicago"

  Scenario: Check appointment date can only be present or future
    Then customer is not able to select any past date for appointment

  Scenario: Check description of the issues field is operable with max 200 characters
    Then customer is able to enter description with max lenght 200 characters

  Scenario: Check description of the issues field shows error on exceeding 200 characters
    Then customer is shown error when enters description with 201 characters

  Scenario: Check user is not able to take already taken appointment
    When customer tried for an occupied appointment
    Then booking of that appointment should be disabled

  Scenario: Check if user is able to take unoccupied appointment
    When customer tries for an appointment not occupied
    Then booking of that appointment should be done
