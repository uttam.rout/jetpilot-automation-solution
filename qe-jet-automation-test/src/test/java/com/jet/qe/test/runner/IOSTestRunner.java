package com.jet.qe.test.runner;

import org.junit.runner.RunWith;

//import cucumber.api.testng.AbstractTestNGCucumberTests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/test/resources/features/ios/",
		glue="com.jet.qe.test.stepdefinition.iso",
		monochrome=true, 
				
				format =	{"pretty","html:target/Destination", "json:target/cucumber.json"}
		)

public class IOSTestRunner {

}
