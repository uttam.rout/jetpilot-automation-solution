package com.jet.qe.test.pageobject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qe.framework.common.CommonActionHelper;

public class LandingPage_PO extends CommonActionHelper {

	/**************** START LOCAL OBJETCS AND DECLARATIONS ***********************/
	private static final Logger logger = Logger.getLogger(LandingPage_PO.class);
//	/***************************** START XPAHTS **********************************/
	
	
	@FindBy(xpath = "//*[@id='mg-notification']//p") 
	public List<WebElement> Lbl_NoMerchGroups;
	
	@FindBy(xpath = "//button[@id='add-range-btn']/span/span") 
	public  WebElement Btn_AddARange;
	
	@FindBy(xpath = "") 
	public WebElement Btn_UserName;
	
	@FindBy(xpath = "//a[text()='Sign In'] | //*[@data-auid='signInCta_m']")
	public WebElement Btn_Password;
 
	@FindBy(xpath = "//button[@id='view-ma-add-range-btn']/span/span") 
	public WebElement Btn_AddOrRemoveRange;
	
	@FindBy(xpath = "//*[text()='To begin, please add a range.']") 
	public WebElement Label_DefaultAssormentMessage;
		
	@FindBy(xpath = "//*[text()='My ranges']") 
	public WebElement Label_ProductHeader;
	
}

