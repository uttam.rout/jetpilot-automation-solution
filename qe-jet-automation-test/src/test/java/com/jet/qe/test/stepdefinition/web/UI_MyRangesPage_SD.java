package com.jet.qe.test.stepdefinition.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.hamcrest.text.IsEqualIgnoringWhiteSpace;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import org.testng.Assert;

import com.jet.qe.test.pageobject.MerchGroupPage_PO;
import com.jet.qe.test.pageobject.MyrangesPage_PO;
import com.qe.framework.common.CommonActionHelper;
import com.qe.framework.web.helpers.WebDriverHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import freemarker.template.utility.NullArgumentException;

public class UI_MyRangesPage_SD extends CommonActionHelper {
	private static final Logger logger = Logger.getLogger(UI_MyRangesPage_SD.class);
	MyrangesPage_PO MyRangesPO = PageFactory.initElements(getDriver(), MyrangesPage_PO.class);
	MerchGroupPage_PO MerchGroupPO = PageFactory.initElements(getDriver(), MerchGroupPage_PO.class);
	public static LinkedHashMap<String, List<String>> myRangePageRangeAndMerchGroupMap = new LinkedHashMap<>();

	@Then("^user verifies already removed junior buyer area \"(.*?)\" at My Range Page$")
	public void user_verifies_already_removed_junior_buyer_area_at_My_Range_Page(String testData, DataTable locatorType)
			throws Throwable {

		String currentElement = null;
		String juniorBuyerArea = null;

		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement-- --> " + currentElement);

				if (currentElement.equalsIgnoreCase("RangeList")) {

					System.out.println("data --->" + webPropHelper.getTestDataProperty(testData));
					juniorBuyerArea = webPropHelper.getTestDataProperty(testData);
					List<WebElement> rangeList = MyRangesPO.RangeNameInList;
					for (WebElement we : rangeList) {

						if (we.getText().equalsIgnoreCase(juniorBuyerArea)) {
							assertFalse(juniorBuyerArea, true);

						} else {
							assertTrue(juniorBuyerArea, true);

						}

					}

				} else if (currentElement.equalsIgnoreCase("Btn_CrossButton")) {

				}
			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^user verifies already added junior buyer area \"(.*?)\" at My Range Page$")
	public void user_verifies_already_added_junior_buyer_area_at_My_Range_Page(String testData, DataTable locatorType)
			throws Throwable {

		String currentElement = null;
		String juniorBuyerArea = null;

		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement-- --> " + currentElement);

				if (currentElement.equalsIgnoreCase("RangeList")) {

					System.out.println("data --->" + webPropHelper.getTestDataProperty(testData));
					juniorBuyerArea = webPropHelper.getTestDataProperty(testData);
					List<WebElement> rangeList = MyRangesPO.RangeNameInList;
					for (WebElement we : rangeList) {

						if (we.getText().equalsIgnoreCase(juniorBuyerArea)) {
							assertTrue(juniorBuyerArea, true);

						}

					}

				} else if (currentElement.equalsIgnoreCase("Btn_CrossButton")) {

				}
			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@And("^user remove all junior buyer areas \"(.*?)\" at My Range page$")
	public void user_remove_all_junior_buyer_areas_at_My_Range_page(String testData, DataTable locatorType)
			throws Throwable {

		String currentElement = null;

		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement-- --> " + currentElement);

				if (currentElement.equalsIgnoreCase("RangeList")) {

					List<WebElement> rangeList = MyRangesPO.Btn_CrossButton_All;

					System.out.println("Size of added rages is " + rangeList.size());
					for (WebElement we : rangeList) {
						Thread.sleep(1000);
						clickOnButton(getDriver().findElement(By.xpath("//h5/*[name()='svg']")));

					}

				} else if (currentElement.equalsIgnoreCase("Btn_CrossButton")) {

				}
			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Given("^user remove junior buyer area \"(.*?)\" at My range page$")
	public void user_remove_junior_buyer_area_at_My_range_page(String testData, DataTable locatorType)
			throws Throwable {

		String currentElement = null;
		String juniorBuyerArea = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement-- --> " + currentElement);

				if (currentElement.equalsIgnoreCase("RangeList")) {

					System.out.println("data --->" + webPropHelper.getTestDataProperty(testData));
					juniorBuyerArea = webPropHelper.getTestDataProperty(testData);
					System.out.println("juniorBuyerArea --->" + juniorBuyerArea);

					List<WebElement> rangeList = MyRangesPO.RangeNameInList;

					System.out.println("Size of added rages is " + rangeList.size());
					for (WebElement we : rangeList) {

						if (we.getText().equalsIgnoreCase(juniorBuyerArea)) {
							clickOnButton(getDriver()
									.findElement(By.xpath("//h5[text()='" + juniorBuyerArea + "']/child::*")));
						}

					}

				} else if (currentElement.equalsIgnoreCase("Btn_CrossButton")) {

				}
			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@When("^User gets all the \"(.*?)\" on the My ranges Page$")
	public void user_gets_all_the_on_the_My_ranges_Page(String testData, DataTable locatorType) throws Throwable {
	     
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);

				System.out.println("currentElement --> " + currentElement);
				
				if (currentElement.equalsIgnoreCase("range_Title")) {
					waitForPageLoad(getDriver());
					List<WebElement> titleMasterAssortment= MyRangesPO.range_Title;
					ArrayList<String> rangeList = new ArrayList<String>();
					List<WebElement> merchGroupElementList;
					Thread.sleep(2000);
					for( WebElement we:titleMasterAssortment)
					    {
					    	rangeList.add(we.getText());
					    	merchGroupElementList = getDriver().findElements(By.xpath("//a[contains(text(),'" + we.getText()+ "')]/..//a[contains(@id,'mg-code')]"));
					    	List<String> merchCodeList = new ArrayList<String>();
					        for (WebElement merchGroupElement: merchGroupElementList)
							 {
					        	 merchCodeList.add(merchGroupElement.getText());
							 }
					        myRangePageRangeAndMerchGroupMap.put(we.getText(), merchCodeList);
					    }
					System.out.println("hmap expected "+myRangePageRangeAndMerchGroupMap);
				}

				else if (currentElement.equalsIgnoreCase("Btn_Cancel")) {
					clickOnButton(MyRangesPO.Btn_Cancel);
				}
			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@When("^User clicks on \"(.*?)\" on the My ranges Page$")
	public void user_clicks_on_the_My_ranges_Page1(String testData, DataTable locatorType) throws Throwable {

		String currentElement = null;
		String mercheCode = null;
		String displayDropdownValue = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);

				System.out.println("currentElement --> " + currentElement);
				if (currentElement.equalsIgnoreCase("Btn_EnableAddRange")) {
					clickOnButton(MyRangesPO.Btn_EnableAddRange);
				}

				else if (currentElement.equalsIgnoreCase("link_merchcode")) {
					Thread.sleep(5000);
					List<WebElement> merchCodeList = MyRangesPO.link_merchcode;
					System.out.println("link_merchcode Size of added rages is " + merchCodeList.size());

					if (isDisplayed(merchCodeList.get(1))) {
						mercheCode = merchCodeList.get(1).getText();

						clickOnButton(merchCodeList.get(1));

						displayDropdownValue = MerchGroupPO.display_Value_Dropdown.getText();

						assertEquals(mercheCode.trim(), displayDropdownValue.trim());

					} else {

						logger.error("No merch group is found");
					}

				}

				else if (currentElement.equalsIgnoreCase("ranges_MasterAssortment")) {
					Thread.sleep(5000);
					List<WebElement> rangeMasterList = MyRangesPO.ranges_MasterAssortment;

					System.out.println(" link_merchcode Size of added rages is " + rangeMasterList.size());

					if (isDisplayed(rangeMasterList.get(1))) {
						mercheCode = rangeMasterList.get(1).getText();

						clickOnButton(rangeMasterList.get(1));

						displayDropdownValue = MerchGroupPO.display_Value_Dropdown.getText();

						assertEquals(mercheCode.trim(), displayDropdownValue.trim());

					} else {

						logger.error("No merch group is found");
					}

				}

				else if (currentElement.equalsIgnoreCase("Btn_DisableAddRange")) {

				}

				else if (currentElement.equalsIgnoreCase("Link_BackToMasterAssortment")) {
					clickOnButton(MyRangesPO.Link_BackToMasterAssortment);
				}

				else if (currentElement.equalsIgnoreCase("Btn_Save")) {

					clickOnButton(MyRangesPO.Btn_Save);

				}

				else if (currentElement.equalsIgnoreCase("Btn_Cancel")) {
					clickOnButton(MyRangesPO.Btn_Cancel);

				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@When("^User clicks on \"(.*?)\" on the My ranges Page and select the value \"(.*?)\"$")
	public void user_clicks_on_on_the_My_ranges_Page_and_select_the_value(String elemet, String testData,
			DataTable locatorType) throws Throwable {
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement-- --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Drpdwn_Category")) {

					System.out.println("data --->" + webPropHelper.getTestDataProperty(testData));
					clickOnButton(MyRangesPO.Drpdwn_Category);
					selectByText(MyRangesPO.Drpdwn_Category, webPropHelper.getTestDataProperty(testData));

				} else if (currentElement.equalsIgnoreCase("Drpdwn_ProdcutArea")) {
					clickOnButton(MyRangesPO.Drpdwn_ProdcutArea);
					selectByText(MyRangesPO.Drpdwn_ProdcutArea, webPropHelper.getTestDataProperty(testData));

				} else if (currentElement.equalsIgnoreCase("Drpdwn_Buyer")) {
					assertTrue(clickOnButton(MyRangesPO.Drpdwn_BuyerArea));
					selectByText(MyRangesPO.Drpdwn_BuyerArea, webPropHelper.getTestDataProperty(testData));

				} else if (currentElement.equalsIgnoreCase("Drpdwn_JuniorBuyerArea")) {

					assertTrue(clickOnButton(MyRangesPO.Drpdwn_JuniorBuyerArea));
					selectByText(MyRangesPO.Drpdwn_JuniorBuyerArea, webPropHelper.getTestDataProperty(testData));

					assertTrue(clickOnButton(MyRangesPO.Drpdwn_JuniorBuyerArea));
					selectByText(MyRangesPO.Drpdwn_JuniorBuyerArea, webPropHelper.getTestDataProperty(testData));
					Thread.sleep(2000);

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}
	}

	@Then("^User verifies the \"(.*?)\" with Junior buyer name under Range list$")
	public void user_verifies_on_Myranges_Page(String testData, DataTable locatorType) throws Throwable {

		VerifyTextPersent(webPropHelper.getTestDataProperty(testData));

	}

	@Then("^User verifies \"(.*?)\" persent on the My ranges page$")
	public void user_verifies_persent_on_the_My_ranges_page(String testData, DataTable locatorType) throws Throwable {

		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Btn_EnableAddRange")) {
					Thread.sleep(2000);
					assertTrue(isDisplayed(MyRangesPO.Btn_EnableAddRange));

				} else if (currentElement.equalsIgnoreCase("Btn_Save")) {
					assertTrue(isDisplayed(MyRangesPO.Btn_Save));
					Thread.sleep(5000);

				}

				else if (currentElement.equalsIgnoreCase("Btn_DisableSave")) {
					assertTrue(isDisplayed(MyRangesPO.Btn_DisableSave));

				}

				else if (currentElement.equalsIgnoreCase("Btn_DisableAddRange")) {
					assertTrue(isDisplayed(MyRangesPO.Btn_DisableAddRange));
				} else if (currentElement.equalsIgnoreCase("Label_Category")) {

					assertTrue(isDisplayed(MyRangesPO.Label_Category));

				} else if (currentElement.equalsIgnoreCase("Label_Productarea")) {

					assertTrue(isDisplayed(MyRangesPO.Label_Productarea));

				} else if (currentElement.equalsIgnoreCase("Label_Buyerarea")) {

					assertTrue(isDisplayed(MyRangesPO.Label_Buyerarea));

				} else if (currentElement.equalsIgnoreCase("Label_Juniorbuyerarea")) {

					assertTrue(isDisplayed(MyRangesPO.Label_Juniorbuyerarea));

				}

				else if (currentElement.equalsIgnoreCase("Label_DefaultAssormentMessage")) {
					VerifyTextPersent(webPropHelper.getTestDataProperty(testData));
				}

				else if (currentElement.equalsIgnoreCase("Tittle_mPRLandingPage")) {

					Assert.assertEquals(getTitle(), " Masterassortment");

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies the \"(.*?)\" number of ranges$")
	public void user_verifies_the_number_of_ranges(String testData, DataTable locatorType) throws Throwable {

		System.out.println("---->" + webPropHelper.getTestDataProperty(testData));
		VerifyTextPersent(webPropHelper.getTestDataProperty(testData));

	}

	@Then("^User verifies the message \"(.*?)\" does not present on My ranges page$")
	public void user_verifies_the_message_does_not_present_on_My_ranges_page(String testData, DataTable locatorType)
			throws Throwable {
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("TD_RangeSaved")) {

					Thread.sleep(3000);
					VerifyTextNotPersent(webPropHelper.getTestDataProperty(testData));

				} else if (currentElement.equalsIgnoreCase("TD_NoRangeMessage")) {

					Thread.sleep(3000);
					VerifyTextNotPersent(webPropHelper.getTestDataProperty(testData));

				}

				else if (currentElement.equalsIgnoreCase("TD_RangeRemoved")) {

					Thread.sleep(3000);
					VerifyTextNotPersent(webPropHelper.getTestDataProperty(testData));

				}

				else if (currentElement.equalsIgnoreCase("TD_AddAndRemoved")) {
					Thread.sleep(3000);
					VerifyTextNotPersent(webPropHelper.getTestDataProperty(testData));

				}

				else if (currentElement.equalsIgnoreCase("TD_NoMerchedGroup")) {

					VerifyTextNotPersent(webPropHelper.getTestDataProperty(testData));

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@And("^User navigate  the currrent page$")
	public void user_navigate_the_currrent_page() throws Throwable {

	}

	@Then("^User verifies the message \"(.*?)\" on My ranges page$")
	public void user_verifies_the_message_on_My_ranges_page(String testData, DataTable locatorType) throws Throwable {

		Thread.sleep(3000);
		VerifyTextPersent(webPropHelper.getTestDataProperty(testData));

	}

	@Then("^User verifies the added \"(.*?)\" on My ranges page$")

	public void user_verifies_the_added_on_My_ranges_page(String testData, DataTable locatorType) throws Throwable {

		String currentElement = null;
		System.out.println("element " + testData);
		System.out.println("locatorType----" + locatorType);
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("List_Added_Range")) {

					List<WebElement> rangeList = MyRangesPO.List_Added_Range;
					System.out.println("Size of added rages is " + rangeList.size());
					Assert.assertTrue(rangeList.size() == 7);
					for (WebElement we : rangeList) {
						System.out.println("we.getText()--->>> " + we.getText());
					}

				}

				else if (currentElement.equalsIgnoreCase("Dis_Drpdwn_ProdcutArea")) {

					Thread.sleep(2000);

					Assert.assertFalse(isEnabled((MyRangesPO.Dis_Drpdwn_ProdcutArea)));

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies dropdown value should be disabled for \"(.*?)\" on the My ranges Pages$")
	public void user_verifies_dropdown_value_should_be_disabled_for_on_the_My_ranges_Pages(String testData,
			DataTable locatorType) throws Throwable {

		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Drpdwn_JuniorBuyerArea")) {

					Thread.sleep(2000);
					Assert.assertTrue(isDisplayed(MyRangesPO.Drpdwn_JuniorBuyerArea,
					webPropHelper.getTestDataProperty(testData)));

				}

				else if (currentElement.equalsIgnoreCase("Dis_Drpdwn_ProdcutArea")) {

					Thread.sleep(2000);

					Assert.assertFalse(isEnabled((MyRangesPO.Dis_Drpdwn_ProdcutArea)));

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies dropdown should be disabled for \"(.*?)\" on the My ranges Pages$")
	public void user_verifies_dropdown_should_be_disabled_for_on_the_My_ranges_Pages(String testData,
			DataTable locatorType) throws Throwable {
		System.out.println("Inside the verification part");
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Drpdwn_Category")) {

					Assert.assertFalse(isEnabled((MyRangesPO.Drpdwn_Category)));

				}

				else if (currentElement.equalsIgnoreCase("Dis_Drpdwn_ProdcutArea")) {

					Assert.assertFalse(isEnabled((MyRangesPO.Dis_Drpdwn_ProdcutArea)));

				}

				else if (currentElement.equalsIgnoreCase("Dis_Drpdwn_Buyer")) {
					Assert.assertFalse(isEnabled((MyRangesPO.Dis_Drpdwn_Buyer)));

				}

				else if (currentElement.equalsIgnoreCase("Dis_Drpdwn_JuniorBuyerArea")) {

					Assert.assertFalse(isEnabled((MyRangesPO.Dis_Drpdwn_JuniorBuyerArea)));

				} else if (currentElement.equalsIgnoreCase("Dis_Drpdwn_JuniorBuyerArea")) {

					Assert.assertFalse(isEnabled((MyRangesPO.Dis_Drpdwn_JuniorBuyerArea)));

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies dropdown should be alphabetic arranged  for \"(.*?)\" on the My ranges Pages$")
	public void user_verifies_dropdown_should_be_alphabetic_arranged_for_on_the_My_ranges_Pages(String arg1,
			DataTable arg2) throws Throwable {

	}

	@Then("^User verifies junior buyer area name under \"(.*?)\" in My Range page$")
	public void user_verifies_junior_buyer_area_name_under_in_My_Range_page(String testData, DataTable locatorType)
			throws Throwable {
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);

				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("RangeNameInList")) {
					ArrayList<String> getList = new ArrayList<String>();
					List<WebElement> rangeList = MyRangesPO.RangeNameInList;
					for (WebElement we : rangeList) {
						getList.add(we.getText());

					}
					System.out.println(getList);

					System.out.println(MyRangesPO.List_Added_Range.size());

				}

				else if (currentElement.equalsIgnoreCase("List_Added_Range")) {

					assertNotNull(MyRangesPO.List_Added_Range.size());

				} else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies junior buyer area name in \"(.*?)\" My Range page$")
	public void user_verifies_junior_buyer_area_name_in_My_Range_page(String testData, DataTable locatorType)
			throws Throwable {

		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("RangeNameInList")) {
					Thread.sleep(2000);
					for (WebElement we : MyRangesPO.RangeNameInList) {
						assertTrue(isDisplayed(we, webPropHelper.getTestDataProperty(testData)));
						getText(we).equals(webPropHelper.getTestDataProperty(testData));
					}

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies dropdown should be alphabetic  for \"(.*?)\" on the My ranges Pages$")
	public void user_verifies_dropdown_should_be_alphabetic_arranged_for_on_the_My_ranges_Page(String testData,
			DataTable locatorType) throws Throwable {
		System.out.println("in side verification --> ");
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Lst_Drpdwn_Category")) {

					ArrayList<String> obtainedList = new ArrayList<String>();
					List<WebElement> elementList = MyRangesPO.Lst_Drpdwn_Category;
					for (WebElement we : elementList) {
						obtainedList.add(we.getText());
					}
					ArrayList<String> sortedList = new ArrayList<>();
					for (String s : obtainedList) {
						sortedList.add(s);
					}
					Collections.sort(sortedList);
					Assert.assertTrue(sortedList.equals(sortedList));

				}

				else if (currentElement.equalsIgnoreCase("Drpdwn_ProdcutArea")) {

				} else if (currentElement.equalsIgnoreCase("Drpdwn_BuyerArea")) {

				} else if (currentElement.equalsIgnoreCase("Drpdwn_JuniorBuyerArea")) {

				} else if (currentElement.equalsIgnoreCase("Drpdwn_JuniorBuyerArea")) {

				} else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	@Then("^User verifies dropdown \"(.*?)\" for \"(.*?)\" on the My ranges Pages$")
	public void user_verifies_dropdown_for_on_the_My_ranges_Page(String arg1, String arg2, DataTable locatorType)
			throws Throwable {

		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Drpdwn_Category")) {
					Thread.sleep(2000);
					assertTrue(isEnabled((MyRangesPO.Drpdwn_Category)));

				}

				else if (currentElement.equalsIgnoreCase("Drpdwn_ProdcutArea")) {
					Thread.sleep(2000);

					assertTrue(isEnabled((MyRangesPO.Drpdwn_ProdcutArea)));

				} else if (currentElement.equalsIgnoreCase("Drpdwn_BuyerArea")) {
					Thread.sleep(2000);
					assertTrue(isEnabled((MyRangesPO.Drpdwn_BuyerArea)));

				} else if (currentElement.equalsIgnoreCase("Drpdwn_JuniorBuyerArea")) {
					Thread.sleep(2000);
					assertTrue(isEnabled((MyRangesPO.Drpdwn_BuyerArea)));

				} else if (currentElement.equalsIgnoreCase("Drpdwn_JuniorBuyerArea")) {
					Thread.sleep(2000);
					assertFalse(isEnabled((MyRangesPO.Drpdwn_JuniorBuyerArea)));

				} else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

}
