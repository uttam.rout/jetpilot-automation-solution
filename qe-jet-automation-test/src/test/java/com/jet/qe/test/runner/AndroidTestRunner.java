package com.jet.qe.test.runner;

import org.junit.runner.RunWith;

//import cucumber.api.testng.AbstractTestNGCucumberTests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/test/resources/features/android/",
		glue="com.jet.qe.test.stepdefinition.android",
		monochrome=true, 
			format =	{"pretty","html:target/Destination", "json:target/cucumber.json"}
		)
public class AndroidTestRunner {

}
