package com.jet.qe.test.common;

import static io.restassured.RestAssured.given;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
//import com.mPR.qe.test.stepdefinition.api.API_GetUserRange_SD;
import com.qe.framework.common.JsonValidation;
import com.qe.framework.common.PropertiesHelper;

import io.restassured.response.Response;

public class CommonTestHelper {
	private static final Logger logger = Logger.getLogger(CommonTestHelper.class);
	private Response response = null;

	public static void main(String[] args) {
		String apiEndpointWebURL = "dmeo";

		System.out.println(getAPIEndpointWebURL(apiEndpointWebURL));

	}

	public static String getAPIEndpointWebURL(String apiEndpointWebURL) {
		if (apiEndpointWebURL != null && apiEndpointWebURL.contains("/?debug=aso")) {
			apiEndpointWebURL = apiEndpointWebURL.substring(0, apiEndpointWebURL.indexOf("/?debug=aso"));
		}
		return apiEndpointWebURL;
	}

	public String getToken(final String tokenType) {
		String accessToken = null;

		try {

			JSONObject request = new JSONObject();
			request.put("client_id", PropertiesHelper.getInstance().getConfigPropProperty("Token_ClientId"));
			request.put("grant_type", PropertiesHelper.getInstance().getConfigPropProperty("Token_ClientGrantType"));
			request.put("username", PropertiesHelper.getInstance().getConfigPropProperty("Token_UserName"));
			request.put("password", PropertiesHelper.getInstance().getConfigPropProperty("Token_Password"));

			response = given().header("content-Type", "application/json").body(request.toString())
					.post(PropertiesHelper.getInstance().getConfigPropProperty("api.tokenURL"));

			accessToken = JsonValidation.getValueFromJSon(response.asString(), tokenType);

		} catch (Exception e) {
			response = null;
			logger.error("initiateRestPostAPICall exception msg::" + e.getMessage());
			e.printStackTrace();
		}
		return accessToken;
	}

}
