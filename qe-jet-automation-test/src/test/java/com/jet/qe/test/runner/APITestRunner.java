package com.jet.qe.test.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)

@CucumberOptions(
		features= {"src/test/resources/features/api/JetPoilt.feature"},
		glue= {"com.jet.qe.test.stepdefinition.api"},
		monochrome=true, 
		format={"pretty","html:target/cucumber-reports/cucumber-html-reports", "json:target/cucumber-reports/cucumber-html-reports/common.json"}
		) 

public class APITestRunner 
{

}

