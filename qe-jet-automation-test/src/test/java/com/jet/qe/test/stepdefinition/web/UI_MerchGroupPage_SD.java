package com.jet.qe.test.stepdefinition.web;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.jet.qe.test.pageobject.MerchGroupPage_PO;
import com.jet.qe.test.pageobject.MyrangesPage_PO;
import com.qe.framework.common.CommonActionHelper;
import com.qe.framework.web.helpers.WebDriverHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import freemarker.template.utility.NullArgumentException;

public class UI_MerchGroupPage_SD extends CommonActionHelper {
	
	private static final Logger logger = Logger.getLogger(UI_MerchGroupPage_SD.class);
	MerchGroupPage_PO MerchGroupPO = PageFactory.initElements(getDriver(), MerchGroupPage_PO.class);
	public static String selectedValue= null;
	public static String selectedRangeValue =null;
	
	@Then("^User verifies \"(.*?)\" persent on the Merch Group page$")
	public void user_verifies_persent_on_the_Merch_Group_page(String testData, DataTable locatorType) throws Throwable {
	    
		String currentElement = null;
		
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				//System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Btn_Back_To_Dashboard")) {
					Thread.sleep(2000);
					assertTrue(isDisplayed(MerchGroupPO.Btn_Back_To_Dashboard));

				} 

				else if (currentElement.equalsIgnoreCase("all_Value_Dropdown"))
				{
					
					List<WebElement> myrangeDropdownList =MerchGroupPO.myRangeListInDropdown;
					LinkedHashMap<String, List<String>> merchGroupPageRangeAndMerchGroupMap = new LinkedHashMap<>();
					String strCurrentMasterAssortment = null;
					
					
					for (WebElement we : myrangeDropdownList)
					{
						try {
							
						   String strCurrentValue = we.getText();
							List<String> lstChild= new ArrayList<String>();
							if (strCurrentValue.endsWith("(Master assortment)"))
							{
								strCurrentMasterAssortment = we.getText();
								merchGroupPageRangeAndMerchGroupMap.put(strCurrentMasterAssortment, lstChild);
							}
							else {
								
								List<String> lstValue = merchGroupPageRangeAndMerchGroupMap.get(strCurrentMasterAssortment);
								if (lstValue!=null)
								lstValue.add(we.getText());
								merchGroupPageRangeAndMerchGroupMap.put(strCurrentMasterAssortment, lstValue);
							}
										
						} catch (Exception e) {
							logger.error("merch code is not present on dropdown.");
							e.printStackTrace();
						}
					}
					System.out.println("merchGroupPageRangeAndMerchGroupMap--->"+merchGroupPageRangeAndMerchGroupMap);	
					
					assertEquals(UI_MyRangesPage_SD.myRangePageRangeAndMerchGroupMap, merchGroupPageRangeAndMerchGroupMap);
					
				}
				
				else if (currentElement.equalsIgnoreCase("current_Page_Title")) {
					
					WebElement myMerchGroupCodeDropdownList =MerchGroupPO.Titie_MerchGroupCode; 
				 assertEquals(myMerchGroupCodeDropdownList.getText(),UI_MerchGroupPage_SD.selectedValue );
				  System.out.println("UI_MerchGroupPage_SD.selectedValue--->"+UI_MerchGroupPage_SD.selectedValue);
					
					
				}	
				
                else if (currentElement.equalsIgnoreCase("current_Range_Page_Title")) {
                	String rangeTitleValue=null;
                	
				WebElement myrangeDropdownList =MerchGroupPO.titie_Range_Page;
			//    rangeTitleValue= UI_MerchGroupPage_SD.selectedRangeValue.replaceAll("(Master assortment)", "");
				
			    System.out.println("UI_MerchGroupPage_SD.selectedRangeValue--?"+UI_MerchGroupPage_SD.selectedRangeValue);
			    System.out.println("myrangeDropdownList.getText()====" + myrangeDropdownList.getText());
			    
						
			    assertTrue((UI_MerchGroupPage_SD.selectedRangeValue).contains(myrangeDropdownList.getText()));
				 
				 System.out.println("UI_MerchGroupPage_SD.selectedRangeValue--->"+UI_MerchGroupPage_SD.selectedRangeValue);
					
					
				}		
				
				
				else if (currentElement.equalsIgnoreCase("Tittle_mPRMerchGroupPage")) {

					Assert.assertEquals(getTitle(), "myProductRange");

				}
				else if (currentElement.equalsIgnoreCase("Label_Dropdown")) {

					assertTrue(isDisplayed(MerchGroupPO.Label_Dropdown));
				}
				else if (currentElement.equalsIgnoreCase("Titie_MerchGroupCode")) {

					assertTrue(isDisplayed(MerchGroupPO.Titie_MerchGroupCode));
				}
				else if (currentElement.equalsIgnoreCase("dropDown_titie_Range_Page")) {

					assertTrue(isDisplayed(MerchGroupPO.dropDown_titie_Range_Page));
				} 
				
				else if (currentElement.equalsIgnoreCase("last_Changed_date")) {

                       assertTrue(isDisplayed(MerchGroupPO.last_Changed_date));


				}
				
				else if (currentElement.equalsIgnoreCase("total_SKU_Details")) {

                    assertTrue(isDisplayed(MerchGroupPO.total_SKU_Details));


				}
				
				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on Merch Group Page.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	
	@And("^User clicks on \"(.*?)\" on the Merch Group Page$")
	public void user_clicks_on_on_the_Merch_Group_Page(String testData, DataTable locatorType) throws Throwable {
		String currentElement = null;
			try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);

				System.out.println("currentElement --> " + currentElement);
				
				if (currentElement.equalsIgnoreCase("Btn_Back_To_Dashboard")) {

					clickOnButton(MerchGroupPO.Btn_Back_To_Dashboard);
				}
				else if (currentElement.equalsIgnoreCase("all_Value_Dropdown"))
				{
					
					clickOnButton(MerchGroupPO.display_Value_Dropdown);	
					Thread.sleep(5000);
					
				}
				
				else if (currentElement.equalsIgnoreCase("single_Range_Value_Dropdown"))
				{
					
					clickOnButton(MerchGroupPO.display_Value_Dropdown);	
					Thread.sleep(5000);
					
					List<WebElement> myrangeDropdownList =MerchGroupPO.myRangeListInDropdown;
					LinkedHashMap<String, List<String>> merchGroupPageRangeAndMerchGroupMap = new LinkedHashMap<>();
				
					for (WebElement we : myrangeDropdownList)
					{
						try {
							
						   String strCurrentValue = we.getText();
							
							if (strCurrentValue.endsWith("(Master assortment)"))
							{
								selectedRangeValue = we.getText();
								clickOnButton(we);
								System.out.println("selectedRangeValue"+selectedRangeValue);
								break;
								
								
							}
							else {
								
								List<String> lstValue = merchGroupPageRangeAndMerchGroupMap.get(selectedRangeValue);
								if (lstValue!=null)
								lstValue.add(we.getText());
								selectedValue=we.getText();
								System.out.println("selecte MerchGrop dValue"+selectedValue);
								clickOnButton(we);
								break;
													
							}
							
										
						} catch (Exception e) {
							logger.error("merch code is not present on dropdown.");
							e.printStackTrace();
						}
					
					
					}
					}
				
				
				else if (currentElement.equalsIgnoreCase("single_Value_Dropdown"))
				{
					
					clickOnButton(MerchGroupPO.display_Value_Dropdown);	
					Thread.sleep(5000);
					
					List<WebElement> myrangeDropdownList =MerchGroupPO.myRangeListInDropdown;
					LinkedHashMap<String, List<String>> merchGroupPageRangeAndMerchGroupMap = new LinkedHashMap<>();
					String strCurrentMasterAssortment = null;
			       
					for (WebElement we : myrangeDropdownList)
					{
						try {
							
						   String strCurrentValue = we.getText();
							List<String> lstChild= new ArrayList<String>();
							if (strCurrentValue.endsWith("(Master assortment)"))
							{
								strCurrentMasterAssortment = we.getText();
								merchGroupPageRangeAndMerchGroupMap.put(strCurrentMasterAssortment, lstChild);
							}
							else {
								
								List<String> lstValue = merchGroupPageRangeAndMerchGroupMap.get(strCurrentMasterAssortment);
								if (lstValue!=null)
								lstValue.add(we.getText());
								selectedValue=we.getText();
								System.out.println("selectedValue"+selectedValue);
								clickOnButton(we);
								break;
													
							}
							
										
						} catch (Exception e) {
							logger.error("merch code is not present on dropdown.");
							e.printStackTrace();
						}
					
					
					
					
				}
				
							
			}
			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}
	
	


}
