package com.jet.qe.test.stepdefinition.api;

import static io.restassured.RestAssured.given;

import org.junit.Assert;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RestServices {
	
	String URL="http://product.apps.jetsdev.psteklabs.com/product/name/jam";
	Response response;

	/**
	 * With Response return type method will give with extraction of JSON
	 * @param endpoint
	 * @return
	 */
	 public static Response doGetRequest(String endpoint) {
	        RestAssured.defaultParser = Parser.JSON;
	        return
	                given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
	                        when().get(endpoint).
	                        then().contentType(ContentType.JSON).extract().response();
	    }
	 
	 /**
	  * Reference: https://jsonplaceholder.typicode.com/users
	  * @param url
	  */
	@Given("^load the url \"([^\"]*)\"")
	public void loadUrlAndVerify(String url) {
		response = doGetRequest(url);

	}
	
	/**
	 * This method will help to hit the URL 
	 */
	 @When("hit url")
	 public void hitLoadedUrl() {
		 response = doGetRequest(URL);
		 
		 
	 }
	 
	 /**
	  * This method will help to validate the JSON specific object values
	  * @param value1
	  * @param value2
	  * @param value3
	  */
	 @Then("Verify the values \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" in json response")
	 public void verifyJsonResponse(String value1, String value2, String value3) {
		 String userNames1 = response.jsonPath().getString("productId");
			Assert.assertEquals(value1, userNames1);
			System.out.println(userNames1);
			
			String userNames2 = response.jsonPath().getString("category");
			Assert.assertEquals(value2, userNames2);
			System.out.println(userNames2);
			
			String userNames3 = response.jsonPath().getString("department");
			Assert.assertEquals(value3, userNames3); 
			System.out.println(userNames3);
	 }
	 
}
