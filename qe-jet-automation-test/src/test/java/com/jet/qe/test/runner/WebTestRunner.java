package com.jet.qe.test.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions
(
		features="src/test/resources/features/web",
		glue="com.jet.qe.test.stepdefinition.web",
		monochrome=true, 
		format={"pretty","html:target/cucumber-reports/cucumber-html-reports", "json:target/cucumber-reports/cucumber-html-reports/common.json"},
		tags = {"@GreatBox"}
		) 
public class WebTestRunner{	
	
}

