package com.jet.qe.test.stepdefinition.web;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.jet.qe.test.common.CommonTestHelper;
import com.jet.qe.test.pageobject.LandingPage_PO;
import com.jet.qe.test.pageobject.MyrangesPage_PO;
import com.qe.framework.common.CommonActionHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import freemarker.template.utility.NullArgumentException;

public class UI_LandingPage_SD extends CommonActionHelper {

	private static final Logger logger = Logger.getLogger(UI_LandingPage_SD.class);
	LandingPage_PO PO_landingPage = PageFactory.initElements(driver, LandingPage_PO.class);
	MyrangesPage_PO PO_myrangesPage = PageFactory.initElements(driver, MyrangesPage_PO.class);
	private final CommonTestHelper commonTestHelper = new CommonTestHelper();

	@Given("^User launches the browser and navigates to \"(.*?)\" Landing Page$")
	public void user_launches_the_browser_and_navigates_to_Landing_Page(String url) throws Throwable {
		initializeDriver();
		openBaseURL(url);
		Cookie accessTokenCookie = new Cookie("OAuth.AccessToken", commonTestHelper.getToken("access_token"));
		Cookie refreshTokenCookie = new Cookie("OAuth.RefreshToken", commonTestHelper.getToken("refresh_token"));
	   	Cookie tpenvCookie = new Cookie("tpenv", "ppe");
		driver.manage().addCookie(accessTokenCookie);
		driver.manage().addCookie(refreshTokenCookie);
		driver.manage().addCookie(tpenvCookie);
		
		Thread.sleep(2000);
		driver.navigate().refresh();
		Thread.sleep(2000);

		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
		waitForPageLoad(driver);
		PO_landingPage = PageFactory.initElements(driver, LandingPage_PO.class);
		PO_myrangesPage = PageFactory.initElements(driver, MyrangesPage_PO.class);

	}

	
	@Given("^User launches the browser and navigates to \"(.*?)\" Main Page$")
	public void user_launches_the_browser_and_navigates_to_Main_Page(String url) throws Throwable 
	{
		initializeDriver();
		openBaseURL(url);
		driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
		waitForPageLoad(driver);
		PO_landingPage = PageFactory.initElements(driver, LandingPage_PO.class);
		PO_myrangesPage = PageFactory.initElements(driver, MyrangesPage_PO.class);

	}
	//btnK
	
	@When("^User perform search on page$")
	public void user_perform_search_on_page() throws Throwable 
	{	
		driver.findElement(By.name("q")).sendKeys("abced");
		Thread.sleep(5000);
		driver.findElement(By.name("btnK")).click();
		Thread.sleep(5000);
	    // Write code here that turns the phrase above into concrete actions
	}

	
	
	
	@When("^User logins with valid \"(.*?)\" and \"(.*?)\"$")
	public void user_logins_with_valid_and(String testdata, String locatorType) throws Throwable {

		setInputText(PO_landingPage.Btn_UserName, webPropHelper.getTestDataProperty(testdata));
		setInputText(PO_landingPage.Btn_Password, webPropHelper.getTestDataProperty(testdata));

	}

	@Then("^User verifies \"(.*?)\" on myProductRange landing Page$")
	public void user_verifies_on_myProductRange_landing_Page(String testData, DataTable locatorType) throws Throwable {

		String currentElement = null;
		System.out.println("verification--inside  ");
		System.out.println("currentElement " + currentElement);
		try {
			List<List<String>> elements = locatorType.raw();
			System.out.println("element size --> " + elements.size());
			for (int i = 0; i < elements.size(); i++) {

				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Btn_AddARange")) {

					assertTrue(isDisplayed(PO_landingPage.Btn_AddARange));
					assertTrue(clickOnButton(PO_landingPage.Btn_AddARange));

				} else if (currentElement.equalsIgnoreCase("Btn_AddOrRemoveRange")) {

					assertTrue(isDisplayed(PO_landingPage.Btn_AddOrRemoveRange));

				} else if (currentElement.equalsIgnoreCase("Label_DefaultAssormentMessage")) {
					Thread.sleep(2000);
					assertTrue(isEnabled(PO_landingPage.Label_DefaultAssormentMessage));
				} else if (currentElement.equalsIgnoreCase("Label_MyRangePages")) {

					Thread.sleep(2000);
					assertTrue(isDisplayed(PO_myrangesPage.Label_MyRangePages));
				} else if (currentElement.equalsIgnoreCase("Label_MyRangePages")) {

					assertTrue(isDisplayed(PO_myrangesPage.Label_RangeList));
				}

				else if (currentElement.equalsIgnoreCase("Label_ProductHeader")) {

					assertTrue(isDisplayed(PO_landingPage.Label_ProductHeader));
				}

				else if (currentElement.equalsIgnoreCase("Tittle_mPRLandingPage")) {

					Assert.assertEquals(getTitle(), "my Product Range");

				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}
	}
	
	

	@When("^User clicks at \"(.*?)\" on myProductRange landing page$")
	public void user_clicks_at_on_myProductRange_landing_page(String testData, DataTable locatorType) throws Throwable {

		{

			String currentElement = null;
			System.out.println("currentElement " + currentElement);
			System.out.println("Click function");
			try {
				List<List<String>> elements = locatorType.raw();
				for (int i = 0; i < elements.size(); i++) {
					currentElement = elements.get(i).get(0);

					
					if (currentElement.equalsIgnoreCase("Btn_AddOrRemoveRange")) {

						clickOnButton(PO_landingPage.Btn_AddOrRemoveRange);
						System.out.println("currentElement --> " + currentElement);

					}

					else if (currentElement.equalsIgnoreCase("Btn_AddARange")) {

						clickOnButton(PO_landingPage.Btn_AddARange);
					}

					else if (currentElement.equalsIgnoreCase("Link_BackToMasterAssortment")) {

						clickOnButton(PO_myrangesPage.Link_BackToMasterAssortment);

					}

					else {
						logger.error("Element " + currentElement + " is not found.");
						throw new NullArgumentException("Element <" + currentElement + "> is not found.");
					}

				}
			} catch (AssertionError a) {
				logger.error("Element " + currentElement + " is not present on screen.");
				throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
			}

			catch (NullPointerException e) {
				logger.error("This test-step has been failed");
			}
		}

	}

	@Then("^User closes the application$")
	public void user_closes_the_application() throws Throwable {
		logger.debug("User closes the application & Browser.............");
		close();
		quitDriver();
	}
	
	
	@And("^User refresh the currrent page$")
	public void user_refresh_the_currrent_page() throws Throwable {
	  
		Thread.sleep(2000);
		driver.navigate().refresh();
		Thread.sleep(3000);
		
	}
	
	@And("^User navigate the current page$")
	public void user_navigate_the_current_page() throws Throwable {
	   
		Thread.sleep(2000);
		driver.navigate().back();
		Thread.sleep(3000);
		
		
	}
	
	@Then("^User verifies the message if no merch groups \"(.*?)\" on Landing page$")
	public void user_verifies_the_message_if_no_merch_groups_on_Landing_page(String testData, DataTable locatorType) throws Throwable {
	  
		String currentElement = null;
		try {
			List<List<String>> elements = locatorType.raw();
			for (int i = 0; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				System.out.println("currentElement --> " + currentElement);

				if (currentElement.equalsIgnoreCase("Lbl_NoMerchGroups")) {

					Thread.sleep(2000);
					for (WebElement we : PO_landingPage.Lbl_NoMerchGroups) {
						assertTrue(isDisplayed(we, webPropHelper.getTestDataProperty(testData)));
						getText(we).equals(webPropHelper.getTestDataProperty(testData));
					}
			
				}

				else {
					logger.error("Element " + currentElement + " is not found.");
					throw new NullArgumentException("Element <" + currentElement + "> is not found.");
				}

			}
		} catch (AssertionError a) {
			logger.error("Element " + currentElement + " is not present on screen.");
			throw new AssertionError("Element <" + currentElement + "> is not present on application page .");
		}

		catch (NullPointerException e) {
			logger.error("This test-step has been failed");
		}

	}

	
	
	

}
