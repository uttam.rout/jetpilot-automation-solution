package com.jet.qe.test.stepdefinition.web;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jet.qe.test.pageobject.GreatBoxPage;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GreatBoxSteps {
	WebDriver driver;
	WebDriverWait wait;
	
	@Given("I open the Great box url")
	public void openGreatBoxUrl() {
		WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10, 50);
        driver.manage().window().maximize();
        driver.get("https://publicis.invisionapp.com/share/5FSS0R07RKN#/screens"); 
	}
	
	@When("I click Signin button from home page")
	public void clickSiginHomeButn() throws InterruptedException {
		GreatBoxPage boxPage = new GreatBoxPage(driver, wait);
		boxPage.clickSignInHomePage();	
	}

	@When("I enter email and password click Sigin button")
	public void clickSigninPage() throws InterruptedException {
		GreatBoxPage boxPage = new GreatBoxPage(driver, wait);
		boxPage.clickSignInPage();
	}
	
	@When("I select the action from the product")
	public void selectThePrdToSchedule() throws InterruptedException {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0,250)");
		GreatBoxPage boxPage = new GreatBoxPage(driver, wait);
		boxPage.clickSelectToSchedule();
	}
	
	@Then("I click Schedule now")
	public void clickScheduleNow() throws InterruptedException {
	GreatBoxPage boxPage = new GreatBoxPage(driver, wait);
		boxPage.clickSchedule();
	}
	
	@Then("I click Add to calendar")
	public void clickAddToCalendar() throws InterruptedException {
		GreatBoxPage boxPage = new GreatBoxPage(driver, wait);
		boxPage.clickAddToCalendar();
	}
	
	@Then("I click cancel Schedule")
	public void clickCancelSchedule() throws InterruptedException {
	GreatBoxPage boxPage = new GreatBoxPage(driver, wait);
		boxPage.clickScheduleCancelNow();
	}
	
	@After
	public void closeBrowsers() {
		driver.close();
	}
}
