package com.jet.qe.test.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GreatBoxPage extends base{

	int thread = 2000;
	
	public GreatBoxPage(WebDriver driver, WebDriverWait waitCondition) {
		super(driver, waitCondition);
	}
	
	@FindBy(how = How.XPATH, using = "//div[@class='hotspots']/a[1]")
    private WebElement signInbutton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='hotspots']/a[1]")
    private WebElement scheduleNow;
	
	@FindBy(how = How.XPATH, using = "//div[@class='hotspots']/a[1]")
    private WebElement scheduleNowCancel;
	
	public GreatBoxPage clickSignInHomePage() throws InterruptedException {
		Thread.sleep(thread);
		signInbutton.isDisplayed();
		signInbutton.click();
		return this;
	}
	
	public GreatBoxPage clickSignInPage() throws InterruptedException {
		Thread.sleep(thread);
		signInbutton.isDisplayed();
		signInbutton.click();
		return this;
	}
	
	public GreatBoxPage clickSelectToSchedule() throws InterruptedException {
		Thread.sleep(thread);
		signInbutton.isDisplayed();
		signInbutton.click();
		return this;
	}
	
	public GreatBoxPage clickSchedule() throws InterruptedException {
		Thread.sleep(thread);
		scheduleNow.isDisplayed();
		scheduleNow.click();
		return this;
	}
	
	public GreatBoxPage clickScheduleCancelNow() throws InterruptedException {
		Thread.sleep(thread);
		scheduleNowCancel.isDisplayed();
		scheduleNowCancel.click();
		return this;
	}
	
	public GreatBoxPage clickAddToCalendar() throws InterruptedException {
		Thread.sleep(thread);
		scheduleNow.isDisplayed();
		scheduleNow.click();
		return this;
	}
}
