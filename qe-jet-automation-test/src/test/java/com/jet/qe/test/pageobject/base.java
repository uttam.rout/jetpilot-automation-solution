package com.jet.qe.test.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class base {

	protected static WebDriverWait wait;
	
	public base(WebDriver driver, WebDriverWait waitCondition) {
		PageFactory.initElements(driver, this);
		this.wait = waitCondition;	
	}
}
