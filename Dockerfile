# FROM maven:alpine     
# RUN mkdir /myvol
# RUN cd /myvol  
# # WORKDIR /usr/src/app       
# ADD ./qe-automation-framework ./myvol/qe-automation-framework
# ADD ./qe-jet-automation-test ./myvol/qe-jet-automation-test
# ADD test_API_run.sh ./myvol
# RUN cd /myvol/qe-automation-framework && mvn clean install
# ENTRYPOINT sh /myvol/test_API_run.sh

FROM maven:alpine     
RUN mkdir /myvol
RUN cd /myvol  
ADD ./qe-automation-framework ./myvol/qe-automation-framework
ADD ./qe-jet-automation-test ./myvol/qe-jet-automation-test
ADD test_API_run.sh ./myvol
RUN cd /myvol/qe-automation-framework && mvn clean install
ENTRYPOINT sh /myvol/test_API_run.sh


# step1
#  make sure proper java and maven is exists
# step 2
#  set the WORKDIR /usr/src/javaapp
# step 3
#  cd qa-automation-framework
# step 4
#  mvn compile
#  mvn clean install
# step 5
#  cd ..
# step 6
#  cd qa-mpr-automation-test
# step 7
#  command to run the test

